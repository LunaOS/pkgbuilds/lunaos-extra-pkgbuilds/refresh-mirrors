#!/usr/bin/env bash

repolist=$(pacman-conf --repo-list)

wait_and_exit() {
    WAIT_TIME="$1"
    EXIT_CODE="$2"

    read -n 1 -s -r -t "$WAIT_TIME" -p "Press any key to exit..."
    exit "$EXIT_CODE"
}

sudo_cmd() {
    if [[ $(id -u) -eq 0 ]]; then
        "$@"
        return $?
    fi

    if [[ -z "$SUDO_AVAILABLE" && -z "$DOAS_AVAILABLE" ]]; then
        if command -v sudo > /dev/null 2>&1 && sudo -l > /dev/null 2>&1; then
            SUDO_AVAILABLE=true
        elif command -v doas > /dev/null 2>&1 && doas doas -C /etc/doas.conf > /dev/null 2>&1; then
            DOAS_AVAILABLE=true
        fi
    fi

    if [[ "$SUDO_AVAILABLE" == "true" ]]; then
        sudo "$@"
    elif [[ "$DOAS_AVAILABLE" == "true" ]]; then
        doas "$@"
    else
        su -c "$*"
    fi
}

# Read the optional config file for automation
[[ -f /etc/default/mirrors ]] && source /etc/default/mirrors

# ========================
# Help
# ========================
help_flag="USAGE: $(basename $0) [function]

functions:
    arch: Refresh Arch mirrors

    chaotic-aur: Refresh Chaotic AUR mirrors

    all: Refresh all mirrors

$(basename "$0") 1.9
A rate-mirrors wrapper
Developed by Boria138 for
the LunaOS distribution.
Based on Refresh Mirrors from RebornOS"

# =========================
# Pacman Sync DataBase
# =========================
refresh_pacman_db() {
    echo "Refresh Pacman Database"
    echo ""
    sudo_cmd pacman -Sy
    ARCH_DATABASE_UPDATE_FAILED="$?"
    echo ""

    if [ "$ARCH_DATABASE_UPDATE_FAILED" -ne 0 ]; then
        echo ""
        echo "ERROR: pacman exited with the error code $ARCH_DATABASE_UPDATE_FAILED..."
    fi
}

# ========================
# Refresh Chaotic AUR Mirrors
# ========================
rank_chaotic_mirrors() {
    TEMP_DIR="/tmp/pacman.d"
    DESTINATION_DIR="/etc/pacman.d"
    MIRRORLIST_FILENAME="chaotic-mirrorlist"
    PER_MIRROR_TIMEOUT="${PER_MIRROR_TIMEOUT:-3000}"
    FALLBACK_PER_MIRROR_TIMEOUT="${FALLBACK_PER_MIRROR_TIMEOUT:-10000}"
    MIN_MIRRORS="${MIN_MIRRORS:-5}"

    TEMP_FILE="$TEMP_DIR/$MIRRORLIST_FILENAME"
    MIRRORLIST_FILE="$DESTINATION_DIR/$MIRRORLIST_FILENAME"

    if echo $repolist | grep "chaotic-aur" >&/dev/null; then
        echo ""
        echo "Ranking Chaotic AUR Mirrors..."
        echo ""

        sudo_cmd bash -c "mkdir -p '$TEMP_DIR' && rm -f '$TEMP_FILE'"

        sudo_cmd rate-mirrors --allow-root --disable-comments-in-file --per-mirror-timeout="$PER_MIRROR_TIMEOUT" --save="$TEMP_FILE" chaotic-aur
        CHAOTIC_MIRROR_REFRESH_FAILED="$?"
        MIRROR_COUNT=$(grep -oe '^Server\s*=\s*http' "$TEMP_FILE" | wc -l)

        if [ "$MIRROR_COUNT" -lt "$MIN_MIRRORS" ]; then
            echo "Only $MIRROR_COUNT mirrors found..."
            echo "Retrying with a longer timeout duration. This will be much slower. Please be patient..."
            echo ""
            sudo_cmd rm -f "$TEMP_FILE"
            sudo_cmd rate-mirrors --allow-root --disable-comments-in-file --per-mirror-timeout="$FALLBACK_PER_MIRROR_TIMEOUT" --save="$TEMP_FILE" chaotic-aur
            CHAOTIC_MIRROR_REFRESH_FAILED="$?"
            MIRROR_COUNT=$(grep -oe '^Server\s*=\s*http' "$TEMP_FILE" | wc -l)
        fi

        if [ "$CHAOTIC_MIRROR_REFRESH_FAILED" -ne 0 ]; then
            echo "ERROR: rate-mirrors exited with the error code $CHAOTIC_MIRROR_REFRESH_FAILED..."
            echo ""
        elif [ "$MIRROR_COUNT" -lt "$MIN_MIRRORS" ]; then
            CHAOTIC_MIRROR_REFRESH_FAILED=-1
            echo "ERROR: Only $MIRROR_COUNT mirrors found even with a longer timeout duration..."
            echo ""
        else
            echo ""
            sudo_cmd cp -f "$TEMP_FILE" "$MIRRORLIST_FILE"
        fi
    else
        echo "Chaotic AUR mirrors not found in pacman.conf skipped"
    fi
}

# ==========================
# Refresh Arch Linux Mirrors
# ==========================
rank_arch_mirrors() {
    TEMP_DIR="/tmp/pacman.d"
    DESTINATION_DIR="/etc/pacman.d"
    MIRRORLIST_FILENAME="mirrorlist"
    PER_MIRROR_TIMEOUT="${PER_MIRROR_TIMEOUT:-3000}"
    ARCH_MIRROR_PROTOCOL="${ARCH_MIRROR_PROTOCOL:-https}"
    FALLBACK_PER_MIRROR_TIMEOUT="${FALLBACK_PER_MIRROR_TIMEOUT:-10000}"
    MIN_MIRRORS="${MIN_MIRRORS:-5}"

    TEMP_FILE="$TEMP_DIR/$MIRRORLIST_FILENAME"
    MIRRORLIST_FILE="$DESTINATION_DIR/$MIRRORLIST_FILENAME"

    sudo_cmd bash -c "mkdir -p '$TEMP_DIR' && rm -f '$TEMP_FILE'"

    echo "Ranking Arch Linux Mirrors..."
    echo ""
    sudo_cmd rate-mirrors --allow-root --disable-comments-in-file --protocol="$ARCH_MIRROR_PROTOCOL" --per-mirror-timeout="$PER_MIRROR_TIMEOUT" --save="$TEMP_FILE" arch
    ARCH_MIRROR_REFRESH_FAILED="$?"
    MIRROR_COUNT=$(grep -oe '^Server\s*=\s*http' "$TEMP_FILE" | wc -l)
    echo ""

    if [ "$MIRROR_COUNT" -lt "$MIN_MIRRORS" ]; then
        echo "Only $MIRROR_COUNT mirrors found..."
        echo "Retrying with a longer timeout duration. This will be much slower. Please be patient..."
        echo ""
        sudo_cmd rm -f "$TEMP_FILE"
        sudo_cmd rate-mirrors --allow-root --disable-comments-in-file --protocol="$ARCH_MIRROR_PROTOCOL" --per-mirror-timeout="$FALLBACK_PER_MIRROR_TIMEOUT" --save="$TEMP_FILE" arch
        ARCH_MIRROR_REFRESH_FAILED="$?"
        MIRROR_COUNT=$(grep -oe '^Server\s*=\s*http' "$TEMP_FILE" | wc -l)
        echo ""
    fi

    if [ "$ARCH_MIRROR_REFRESH_FAILED" -ne 0 ]; then
        echo "ERROR: rate-mirrors exited with the error code $ARCH_MIRROR_REFRESH_FAILED..."
        echo ""
    elif [ "$MIRROR_COUNT" -lt "$MIN_MIRRORS" ]; then
        ARCH_MIRROR_REFRESH_FAILED=-1
        echo "ERROR: Only $MIRROR_COUNT mirrors found even with a longer timeout duration..."
        echo ""
    else
        echo ""
        sudo_cmd cp -f "$TEMP_FILE" "$MIRRORLIST_FILE"
    fi
}

case "${1}" in
    arch)
        rank_arch_mirrors
        refresh_pacman_db
        ;;
    chaotic|chaotic-aur)
        rank_chaotic_mirrors
        refresh_pacman_db
        ;;
    all)
        rank_arch_mirrors
        rank_chaotic_mirrors
        refresh_pacman_db
        ;;
    -h | --help)
        echo "$help_flag"
        exit 0
        ;;
    *)
        rank_arch_mirrors
        rank_chaotic_mirrors
        refresh_pacman_db
        ;;
esac

# =========================
# Check exit codes and exit
# =========================
if [ ! -z "$CHAOTIC_MIRROR_REFRESH_FAILED" ] && [ "$CHAOTIC_MIRROR_REFRESH_FAILED" -ne 0 ]; then
    >&2 echo ""
    >&2 echo "ERROR: Refresh of Chaotic AUR mirrors failed with exit code: $CHAOTIC_MIRROR_REFRESH_FAILED"
    wait_and_exit 20 "$CHAOTIC_MIRROR_REFRESH_FAILED"
elif [ ! -z "$ARCH_MIRROR_REFRESH_FAILED" ] && [ "$ARCH_MIRROR_REFRESH_FAILED" -ne 0 ]; then
    >&2 echo ""
    >&2 echo "ERROR: Refresh of Arch Linux mirrors failed with exit code: $ARCH_MIRROR_REFRESH_FAILED"
    wait_and_exit 20 "$ARCH_MIRROR_REFRESH_FAILED"
elif [ ! -z "$ARCH_DATABASE_UPDATE_FAILED" ] && [ "$ARCH_DATABASE_UPDATE_FAILED" -ne 0 ]; then
    >&2 echo ""
    >&2 echo "ERROR: Update of Arch Linux mirrors failed with exit code: $ARCH_DATABASE_UPDATE_FAILED"
    wait_and_exit 20 "$ARCH_DATABASE_UPDATE_FAILED"
else
    echo ""
    echo "Refresh mirrors completed successfully!"
    exit 0
fi
